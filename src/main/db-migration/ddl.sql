CREATE TABLE user
(
    id               BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    created          DATE,
    updated          DATE,
    version          INTEGER,
    name             VARCHAR(255) NOT NULL,
    email            VARCHAR(255) NOT NULL UNIQUE,
    password         VARCHAR(255) NOT NULL,
    should_change_password BOOLEAN,
    phone            VARCHAR(255)
);

CREATE TABLE event_type
(
    id          BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    created     DATE,
    updated     DATE,
    version     INTEGER,
    name        VARCHAR(255) NOT NULL UNIQUE,
    description LONGTEXT
);

CREATE TABLE activity
(
    id            BIGINT    NOT NULL AUTO_INCREMENT PRIMARY KEY,
    created       DATE,
    updated       DATE,
    version       INTEGER,
    note          LONGTEXT,
    user_id       BIGINT    NOT NULL,
    event_type_id BIGINT    NOT NULL,
    start_time    TIMESTAMP NOT NULL,
    end_time      TIMESTAMP NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id),
    FOREIGN KEY (event_type_id) REFERENCES event_type (id)
);

CREATE TABLE user_role
(
    user_id BIGINT       NOT NULL,
    role    VARCHAR(255) NOT NULL,
    CONSTRAINT unique_user_role UNIQUE (user_id, role),
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE related_person
(
    activity_id BIGINT NOT NULL,
    user_id     BIGINT NOT NULL,
    PRIMARY KEY (activity_id, user_id),
    FOREIGN KEY (activity_id) REFERENCES activity(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);