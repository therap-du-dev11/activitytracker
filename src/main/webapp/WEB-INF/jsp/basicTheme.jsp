<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<html>
<head>
    <title>
        <decorator:title/>
    </title>
    <link rel="stylesheet" href="<c:url value="/webjars/bootstrap/5.1.1/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/assets/css/custom-2.0.2.css"/>">
    <script src="<c:url value="/webjars/jquery/3.6.0/jquery.min.js"/>"></script>
</head>
<body class="bg-secondary text-white">
<nav class="navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand ms-5" href="<c:url value="/home"/>">
        <fmt:message key="label.home"/>
    </a>
    <div class="collapse navbar-collapse" id="navbarsExample02">
        <ul class="navbar-nav me-auto">
            <c:if test="${not empty USER}">
                <c:if test="${isAdmin}">
                    <li class="nav-item active">
                        <a class="nav-link" href="<c:url value="/user/list"/>">
                            <fmt:message key="btn.user"/>
                        </a>
                    </li>
                </c:if>
                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/eventType/list"/>">
                        <fmt:message key="btn.eventType"/>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/activity/list"/>">
                        <fmt:message key="btn.activities"/>
                    </a>
                </li>
                <c:choose>
                    <c:when test="${isAdmin}">
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/summary/list"/>">
                                <fmt:message key="btn.summary"/>
                            </a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/summary/view"/>">
                                <fmt:message key="btn.summary"/>
                            </a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </ul>
        <ul class="navbar-nav my-2 my-md-0 me-5">
            <c:choose>
                <c:when test="${not empty USER}">
                    <li class="nav-item">
                        <a href="<c:url value="/password/change"/>" class="nav-link">
                            <fmt:message key="btn.passwd.change"/>
                        </a>
                    </li>
                    <li class="nav-item">
                        <c:url var="profileUrl" value="/user/action">
                            <c:param name="id" value="${USER.id}"/>
                        </c:url>
                        <a href="<c:url value="${profileUrl}"/>" class="nav-link">
                            <fmt:message key="btn.profile"/>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/auth/logout"/>">
                            <fmt:message key="btn.logout"/>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/auth/login"/>">
                            <fmt:message key="link.login"/>
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</nav>
<div class="w-100 d-flex justify-content-center">
    <div>
        <c:if test="${not empty warningRMsg}">
            <div class="bg-warning text-dark">
                <c:out value="${warningRMsg}"/>
            </div>
        </c:if>
        <c:if test="${not empty successRMsg}">
            <div class="bg-success text-white">
                <c:out value="${successRMsg}"/>
            </div>
        </c:if>
        <c:if test="${not empty errorRMsg}">
            <div class="bg-danger text-white">
                <c:out value="${errorRMsg}"/>
            </div>
        </c:if>
    </div>
</div>
<decorator:body/>
</body>
</html>