<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="title.activity"/>
    </title>
</head>
<body>
<div class="c-fscrn c-center">
    <div class="c-dark p-4 rounded paper-shadow">
        <form:form modelAttribute="activityCommand">
            <table class="c-table">
                <tr>
                    <th><fmt:message key="label.time.start"/></th>
                    <th>
                        <div>
                            <div>
                                <form:input
                                        path="activity.startTime"
                                        readonly="${activityCommand.readOnly}"
                                        type="datetime-local"/>
                            </div>
                            <div>
                                <form:errors path="activity.startTime" cssClass="text-danger"/>
                            </div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th><fmt:message key="label.time.end"/></th>
                    <th>
                        <div>
                            <div>
                                <form:input
                                        path="activity.endTime"
                                        readonly="${activityCommand.readOnly}"
                                        type="datetime-local"/>
                            </div>
                            <div>
                                <form:errors path="activity.endTime" cssClass="text-danger"/>
                            </div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th><fmt:message key="label.note"/></th>
                    <th>
                        <div>
                            <div><form:input path="activity.note" readonly="${activityCommand.readOnly}"/></div>
                            <div><form:errors path="activity.note"/></div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th><fmt:message key="label.eventType"/></th>
                    <th>
                        <div>
                            <div>
                                <form:select path="activity.eventType" disabled="${activityCommand.readOnly}"
                                             cssClass="rounded">
                                    <fmt:message var="choose" key="label.choose"/>
                                    <form:option value="${null}" label="${choose}"/>
                                    <c:forEach var="eventType" items="${eventTypes}">
                                        <form:option value="${eventType}" label="${eventType.name}"/>
                                    </c:forEach>
                                </form:select>
                            </div>
                            <div>
                                <form:errors path="activity.eventType" cssClass="text-danger"/>
                            </div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th><fmt:message key="label.relatedPerson"/></th>
                    <th>
                        <div>
                            <div id="container">
                                <c:forEach var="person" items="${activityCommand.activity.relatedPersons}">
                                    <form:checkbox path="activity.relatedPersons"
                                                   disabled="${activityCommand.readOnly}"
                                                   value="${person}"
                                                   label="${person.email}"/>
                                </c:forEach>
                            </div>
                            <c:if test="${!activityCommand.readOnly}">
                                <div>
                                    <input name="relatedPersonEmail" type="email" id="email-input">
                                    <div id="add-btn" class="btn btn-primary">
                                        <fmt:message key="btn.plus"/>
                                    </div>
                                    <span class="c_d-none text-danger" id="error-msg">
                                            <fmt:message key="email.invalid"/>
                                        </span>
                                </div>
                            </c:if>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="<c:out value="${activityCommand.readOnly ? '1' : '2'}"/>">
                        <div class="w-100 c-center">
                            <c:choose>
                                <c:when test="${activityCommand.readOnly}">
                                    <c:url var="editUrl" value="/activity/action">
                                        <c:param name="_action_edit"/>
                                    </c:url>
                                    <a href="<c:url value="${editUrl}"/>" class="btn btn-primary">
                                        <fmt:message key="btn.edit"/>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <form:button name="_action_saveOrUpdate" type="success" class="btn btn-success">
                                        <c:choose>
                                            <c:when test="${activityCommand.activity.isNew()}">
                                                <fmt:message key="btn.save"/>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="btn.update"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </form:button>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </th>
                    <c:if test="${activityCommand.readOnly}">
                        <th>
                            <div class="c-center">
                                <form:button name="_action_delete" type="success" class="btn btn-warning">
                                    <fmt:message key="btn.delete"/>
                                </form:button>
                            </div>
                        </th>
                    </c:if>
                </tr>
            </table>
        </form:form>
    </div>
</div>
<script type="text/javascript">
    function exists(emails, email) {
        for (let i = 0; i < emails.length; i++) {
            if (emails[i] === email) {
                return true;
            }
        }
        return false;
    }

    $(document).ready(function () {
        let emails = [];
        $('#add-btn').on('click', function () {
            let email = $('#email-input').val();
            let errorSpan = $('#error-msg');
            let fd = new FormData();
            fd.append('email', email);
            let xhr = new XMLHttpRequest();
            xhr.open('GET', '/rest/user/exists');
            console.log(xhr.getAllResponseHeaders())
            xhr.send(fd);
            if (exists(emails, email)) {
                return;
            }
            xhr.onloadend = function () {
                if (xhr.response === 'false') {
                    errorSpan.css('display', 'block');
                    return;
                }
                emails = emails.concat(email);
                errorSpan.css('display', 'none');
                let container = $('#container');
                let checkbox = document.createElement('input');
                let parentDiv = document.createElement('div');
                let label = document.createElement('label');
                checkbox.setAttribute('type', 'checkbox');
                checkbox.setAttribute('name', 'activity.relatedPersons');
                checkbox.setAttribute('id', email);
                checkbox.setAttribute('value', email);
                checkbox.setAttribute('checked', 'checked');
                label.innerHTML = email;
                label.setAttribute('for', email);
                parentDiv.append(checkbox);
                parentDiv.append(label);
                container.append(parentDiv);
            }
        });
    });
</script>
</body>
</html>