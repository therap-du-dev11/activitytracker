<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="title.eventType.list"/>
    </title>
</head>
<body>
<div class="c-center">
    <div>
        <c:if test="${isAdmin}">
            <div class="w-100 c-center">
                <a class="btn btn-primary mt-3" href="<c:url value="/eventType/action"/>">
                    <fmt:message key="btn.add"/>
                </a>
            </div>
        </c:if>
        <div>
            <c:if test="${empty eventTypes}">
                <fmt:message key="warning.eventType.notFound"/>
            </c:if>
            <c:if test="${not empty eventTypes}">
                <div class="w-100 mt-2">
                    <div class="c-center">
                        <fmt:message key="title.eventType.list"/>
                    </div>
                </div>
                <div class="w-100 mt-3">
                    <div class="w-50 c-center">
                        <fmt:message key="label.nameCn"/>
                    </div>
                    <div class="w-50"></div>
                </div>
                <table class="c-table c-dark rounded">
                    <c:forEach var="eventType" items="${eventTypes}">
                        <tr>
                            <th><c:out value="${eventType.name}"/></th>
                            <th class="bg-opacity-75">
                                <c:url var="detailsUrl" value="/eventType/action">
                                    <c:param name="id" value="${eventType.id}"/>
                                </c:url>
                                <a href="<c:url value="${detailsUrl}"/>" class="btn btn-primary">
                                    <fmt:message key="btn.details"/>
                                </a>
                            </th>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>