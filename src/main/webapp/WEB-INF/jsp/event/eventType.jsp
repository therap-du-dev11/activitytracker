<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="title.saveOrUpdate"/>
    </title>
</head>
<body>
<div class="c-fscrn c-center">
    <form:form modelAttribute="eventTypeCommand">
        <div class="c-dark rounded m-3 p-4 paper-shadow">
            <div class="w-100 c-center mb-4">
                <div>
                    <div>
                        <form:input
                                path="eventType.name"
                                readonly="${eventTypeCommand.readOnly}"
                                cssClass="rounded"
                                placeholder="enter event name"/>
                    </div>
                    <div><form:errors path="eventType.name" cssClass="text-danger"/></div>
                </div>
            </div>
            <div class="c-center cw-300">
                <form:textarea
                        path="eventType.description"
                        readonly="${eventTypeCommand.readOnly}"
                        cols="60"
                        rows="6"
                        cssClass="rounded"
                        placeholder="enter description"/>
            </div>
            <div class="w-100 c-center mt-4">
                <c:if test="${isAdmin}">
                    <c:choose>
                        <c:when test="${eventTypeCommand.readOnly}">
                            <c:url var="editUrl" value="/eventType/action">
                                <c:param name="_action_edit"/>
                            </c:url>
                            <a href="<c:url value="${editUrl}"/>" type="success" class="me-5 btn btn-primary">
                                <fmt:message key="btn.edit"/>
                            </a>
                            <form:button name="_action_delete" class="btn btn-danger" type="success">
                                <fmt:message key="btn.delete"/>
                            </form:button>
                        </c:when>
                        <c:otherwise>
                            <form:button name="_action_saveOrUpdate" type="success" class="btn btn-success">
                                <c:choose>
                                    <c:when test="${eventTypeCommand.eventType.isNew()}">
                                        <fmt:message key="btn.save"/>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="btn.update"/>
                                    </c:otherwise>
                                </c:choose>
                            </form:button>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>