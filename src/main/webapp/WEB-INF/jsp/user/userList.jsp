<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="title.user.list"/>
    </title>
</head>
<body>
<div class="w-100 d-flex justify-content-center">
    <div>
        <div id="filter-div" class="d-flex justify-content-center mt-3">
            <div>
                <form action="<c:url value="/user/list"/>">
                    <input
                            name="searchString"
                            class="p-1 rounded"
                            type="search"
                            placeholder="search by name"
                            id="name"
                            value="<c:out value="${searchString}"/>">
                    <input
                            type="hidden"
                            name="pageNo"
                            id="pageNo"
                            value="<c:out value="${pageNo}"/>">
                    <input type="submit" id="submit" class="p-1" value="<fmt:message key="btn.search"/>">
                </form>
            </div>
            <div>
                <a class="btn btn-primary ms-5" href="<c:url value="/user/action"/>">
                    <fmt:message key="btn.add.user"/>
                </a>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div>
                <c:if test="${empty users}">
                    <fmt:message key="info.warning.users.empty"/>
                </c:if>
                <c:if test="${not empty users}">
                    <div class="w-100 mt-2">
                        <div class="c-center">
                            <fmt:message key="title.user.list"/>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="cw-200 d-flex justify-content-center">
                            <fmt:message key="label.nameCn"/>
                        </div>
                        <div class="cw-200 d-flex justify-content-center">
                            <fmt:message key="label.emailCn"/>
                        </div>
                        <div class="cw-200 d-flex justify-content-center">
                            <fmt:message key="label.phoneCn"/>
                        </div>
                        <div class="cw-200">
                        </div>
                    </div>
                    <c:forEach var="user" items="${users}">
                        <div class="d-flex m-2">
                            <div class="cw-200 rounded-start bg-opacity-75 bg-dark text-light d-flex align-items-center justify-content-center">
                                <c:out value="${user.name}"/>
                            </div>
                            <div class="cw-200  bg-dark text-light d-flex align-items-center justify-content-center">
                                <c:out value="${user.email}"/>
                            </div>
                            <div class="cw-200 bg-opacity-75 bg-dark text-light d-flex align-items-center justify-content-center">
                                <c:out value="${user.phone}"/>
                            </div>
                            <div class="cw-200 rounded-end bg-dark text-light d-flex align-items-center justify-content-center">
                                <c:url var="detailsUrl" value="/user/action">
                                    <c:param name="id" value="${user.id}"/>
                                </c:url>
                                <a class="btn btn-primary m-2" href="<c:url value="${detailsUrl}"/>">
                                    <fmt:message key="btn.details"/>
                                </a>
                            </div>
                        </div>
                    </c:forEach>
                </c:if>
                <div class="w-100 c-center">
                    <c:if test="${hasPrev}">
                        <button id="prev" class="btn-success me-5">
                            <fmt:message key="btn.prev"/>
                        </button>
                    </c:if>
                    <span id="page-state" class="me-5" class="bg-dark text-white">
                                <fmt:message key="label.page"/>
                                <c:out value="${pageNoToView}"/>
                            </span>
                    <c:if test="${hasNext}">
                        <button id="next" class="btn-success">
                            <fmt:message key="btn.next"/>
                        </button>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#prev').on('click', function () {
            let pageInput = $('#pageNo');
            if (pageInput.val() > 0) {
                pageInput.prop('value', parseInt(pageInput.val()) - 1);
                $('#submit').click();
            }
        });
        $('#next').on('click', function () {
            let pageInput = $('#pageNo');
            pageInput.prop('value', parseInt(pageInput.val()) + 1);
            $('#submit').click();
        });
        $('#name').on('change', function () {
            let pageInput = $('#pageNo');
            pageInput.prop('value', 0);
        })
    });
</script>
</body>
</html>