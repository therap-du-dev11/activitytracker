<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <c:out value="${userCommand.user.name}"/>
    </title>
</head>
<body>
<div class="d-flex justify-content-center align-items-center w-100 h-100">
    <div class="bg-dark text-white p-5 paper-shadow rounded">
        <form:form modelAttribute="userCommand">
            <table style="border-spacing: 30px 30px;border-collapse: separate">
                <tr>
                    <th><fmt:message key="label.nameCn"/></th>
                    <th>
                        <div>
                            <div><form:input path="user.name" readonly="${userCommand.readOnly}" type="text"
                                             placeholder="Enter your name"/></div>
                            <div><form:errors path="user.name" cssClass="text-danger"/></div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th><fmt:message key="label.emailCn"/></th>
                    <th>
                        <div>
                            <div><form:input path="user.email" readonly="${userCommand.readOnly}" type="email"
                                             placeholder="Enter your email"/></div>
                            <div><form:errors path="user.email" cssClass="text-danger"/></div>
                        </div>
                    </th>
                </tr>
                <c:if test="${userCommand.user.isNew() && isAdmin}">
                    <tr>
                        <th><fmt:message key="label.passwordCn"/></th>
                        <th>
                            <div>
                                <div><form:password path="user.password" id="password"
                                                    placeholder="Enter password"/></div>
                                <div><form:errors path="user.password" cssClass="text-danger"/></div>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th><fmt:message key="label.confirmPasswordCn"/></th>
                        <th>
                            <div>
                                <div><input id="confirm-password" type="password" placeholder="confirm password"/></div>
                                <div><span id="message"></span></div>
                            </div>
                        </th>
                    </tr>
                </c:if>
                <c:if test="${isAdmin}">
                    <th><fmt:message key="label.roleCn"/></th>
                    <th>
                        <div>
                            <div>
                                <c:forEach var="role" items="${roles}">
                                    <form:checkbox path="user.roles" disabled="${userCommand.readOnly}" value="${role}"
                                                   label="${role.displayName}"/>
                                </c:forEach>
                            </div>
                            <div>
                                <form:errors path="user.roles" cssClass="text-danger"/>
                            </div>
                        </div>
                    </th>
                </c:if>
                <tr>
                    <th><fmt:message key="label.phoneCn"/></th>
                    <th>
                        <div>
                            <div><form:input path="user.phone" readonly="${userCommand.readOnly}"
                                             placeholder="Enter phone no"/></div>
                            <div><form:errors path="user.phone" cssClass="text-danger"/></div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="<c:out value="${userCommand.readOnly ? '1' : '2'}"/>">
                        <div class="d-flex justify-content-center">
                            <c:choose>
                                <c:when test="${userCommand.readOnly}">
                                    <c:url var="editUrl" value="/user/action">
                                        <c:param name="_action_edit"/>
                                    </c:url>
                                    <a href="<c:url value="${editUrl}"/>" class="btn btn-primary">
                                        <fmt:message key="btn.edit"/>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <form:button id="submit-btnn" name="_action_saveOrUpdate"
                                                 class="btn btn-success w-100"
                                                 type="success">
                                        <c:choose>
                                            <c:when test="${userCommand.user.isNew()}">
                                                <fmt:message key="btn.save"/>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="btn.update"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </form:button>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </th>
                    <c:if test="${userCommand.readOnly && !self}">
                        <th>
                            <form:button name="_action_delete" type="success" class="btn btn-warning">
                                <fmt:message key="btn.delete"/>
                            </form:button>
                        </th>
                    </c:if>
                </tr>
            </table>
        </form:form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#confirm-password,#password').on('keyup', function () {
            if ($('#password').val() === $('#confirm-password').val()) {
                $('#message').html('Matching').css('color', 'green');
                $('#submit-btnn').prop('disabled', false);
            } else {
                $('#message').html('Not Matching').css('color', 'red');
                $('#submit-btnn').prop('disabled', true)
            }
        });
    });
</script>
</body>
</html>