<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cstm" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <title>
        <fmt:message key="title.password.change"/>
    </title>
</head>
<body>
<div class="c-center c-fscrn">
    <div class="c-dark paper-shadow rounded">
        <form:form modelAttribute="passwordCommand">
            <table class="m-3 c-table">
                <tr>
                    <th><fmt:message key="label.password.old"/></th>
                    <th>
                        <div>
                            <div>
                                <form:input
                                        path="oldPassword"
                                        cssClass="rounded"
                                        placeholder="enter old password"
                                        type="password"/>
                            </div>
                            <div><form:errors path="oldPassword" cssClass="text-danger"/></div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th><fmt:message key="label.password.new"/></th>
                    <th>
                        <div>
                            <div>
                                <form:input
                                        path="newPassword"
                                        cssClass="rounded"
                                        placeholder="enter new password"
                                        type="password"/>
                            </div>
                            <div><form:errors path="newPassword" cssClass="text-danger"/></div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th><fmt:message key="label.password.confirm"/></th>
                    <th>
                        <div>
                            <div>
                                <form:input
                                        path="confirmPassword"
                                        cssClass="rounded"
                                        placeholder="confirm password"
                                        type="password"/>
                            </div>
                            <div><form:errors path="confirmPassword" cssClass="text-danger"/></div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="2">
                        <div class="c-center w-100">
                            <form:button type="success" class="btn btn-success">
                                <fmt:message key="btn.passwd.change"/>
                            </form:button>
                        </div>
                    </th>
                </tr>
            </table>
        </form:form>
    </div>
</div>
</body>
</html>