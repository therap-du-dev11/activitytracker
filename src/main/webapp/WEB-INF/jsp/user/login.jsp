<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="btn.login"/>
    </title>
</head>
<body>
<div>
    <div style="margin-top: 100px" class="w-100 d-flex justify-content-center">
        <div class="d-flex justify-content-center bg-dark p-5 rounded paper-shadow text-white">
            <form:form modelAttribute="loginCommand" cssClass="d-flex flex-column justify-content-center">
                <div>
                    <div class="d-flex justify-content-center mb-4"><fmt:message key="label.login.req"/></div>
                    <div class="text-danger">
                        <c:if test="${not empty errorMsg}">
                            <c:out value="${errorMsg}"/>
                        </c:if>
                    </div>
                    <div><fmt:message key="label.emailCn"/></div>
                    <div>
                        <form:input
                                path="email"
                                type="email"
                                placeholder="enter user email"
                                class="border-left-0 border-right-0 border-top-0 rounded mt-2"/>
                    </div>
                    <div class="mb-3"><form:errors path="email" cssClass="text-danger"/></div>
                    <div><fmt:message key="label.passwordCn"/></div>
                    <div>
                        <form:password
                                path="password"
                                placeholder="enter password"
                                class="border-right-0 border-left-0 border-top-0 rounded mt-2"/>
                    </div>
                    <div><form:errors path="password" cssClass="text-danger"/></div>
                    <div class="d-flex justify-content-center mt-4">
                        <form:button type="success" class="w-100 btn btn-success p-1 ps-5 pe-5 rounded mt-1 mb-2">
                            <fmt:message key="btn.login"/>
                        </form:button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>