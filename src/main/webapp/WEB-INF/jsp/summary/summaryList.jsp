<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="label.summaries"/>
    </title>
</head>
<body>
<div class="w-100 d-flex justify-content-center">
    <div>
        <div id="filter-div" class="d-flex justify-content-center mt-3">
            <div>
                <form action="<c:url value="/summary/list"/>">
                    <label for="startTime">
                        Start:
                    </label>
                    <input name="startTime"
                           class="p-1 rounded"
                           type="datetime-local"
                           pattern=""
                           value="<c:out value="${startTime}"/>"/>
                    <label for="endTime" class="ms-3">
                        End:
                    </label>
                    <input name="endTime"
                           value="<c:out value="${endTime}"/>"
                           class="p-1 rounded"
                           type="datetime-local">
                    <input type="hidden" name="pageNo"
                           id="pageNo"/>
                    <button type="submit" id="submit" class="p-1 btn btn-primary">
                        <fmt:message key="btn.search"/>
                    </button>
                </form>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div>
                <c:if test="${empty summaries}">
                    <fmt:message key="info.warning.summary.empty"/>
                </c:if>
                <c:if test="${not empty summaries}">
                    <div class="w-100 mt-2">
                        <div class="c-center">
                            <fmt:message key="heading.summaries"/>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="cw-200 d-flex justify-content-center">
                            <fmt:message key="label.user.email"/>
                        </div>
                        <div class="cw-200 d-flex justify-content-center">
                            <fmt:message key="label.eventCount"/>
                        </div>
                        <div class="cw-200 d-flex justify-content-center">
                            <fmt:message key="label.averageTimeInHour"/>
                        </div>
                        <div class="cw-200">
                        </div>
                    </div>
                    <c:forEach var="summary" items="${summaries}">
                        <div class="d-flex m-2">
                            <div class="cw-200 rounded-start bg-opacity-75 bg-dark text-light d-flex align-items-center justify-content-center">
                                <c:out value="${summary.userEmail}"/>
                            </div>
                            <div class="cw-200  bg-dark text-light d-flex align-items-center justify-content-center">
                                <c:out value="${summary.eventCount}"/>
                            </div>
                            <div class="cw-200 bg-opacity-75 bg-dark text-light d-flex align-items-center justify-content-center">
                                <fmt:formatNumber
                                        value="${summary.avgEventTimeInHour}"
                                        maxFractionDigits="3"
                                        minFractionDigits="3"
                                        minIntegerDigits="1"/>
                            </div>
                            <div class="cw-200 rounded-end bg-dark text-light d-flex align-items-center justify-content-center">
                                <c:url var="detailUrl" value="/summary/view">
                                    <c:param name="userId" value="${summary.userId}"/>
                                    <c:param name="startTime" value="${startTime}"/>
                                    <c:param name="endTime" value="${endTime}"/>
                                </c:url>
                                <a class="btn btn-primary m-2" href="<c:url value="${detailUrl}"/>">
                                    <fmt:message key="btn.details"/>
                                </a>
                            </div>
                        </div>
                    </c:forEach>
                </c:if>
                <div class="w-100 c-center">
                    <c:if test="${hasPrev}">
                        <button id="prev" class="btn-success me-5">
                            <fmt:message key="btn.prev"/>
                        </button>
                    </c:if>
                    <span id="page-state" class="me-5" class="bg-dark text-white">
                                <fmt:message key="label.page"/>
                                <c:out value="${pageNoToView}"/>
                            </span>
                    <c:if test="${hasNext}">
                        <button id="next" class="btn-success">
                            <fmt:message key="btn.next"/>
                        </button>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>