<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="title.summary"/>
    </title>
</head>
<body>
<div class="w-100">
    <div id="filter-div" class="d-flex justify-content-center mt-3">
        <div>
            <form action="<c:url value="/summary/view"/>">
                <label for="startTime">
                    Start:
                </label>
                <input name="startTime"
                       class="p-1 rounded"
                       type="datetime-local"
                       value="<c:out value="${startTime}"/>"/>
                <label for="endTime" class="ms-3">
                    End:
                </label>
                <input name="endTime"
                       id="endTime"
                       value="<c:out value="${endTime}"/>"
                       class="p-1 rounded"
                       type="datetime-local"/>
                <input type="hidden" name="userId" value="<c:out value="${userId}"/>">
                <button type="submit" id="submit" class="p-1 btn btn-primary">
                    <fmt:message key="btn.search"/>
                </button>
            </form>
        </div>
    </div>
</div>
<div class="c-center w-100 mt-5">
    <div class="c-dark p-4 rounded paper-shadow">
        <div class="ps-4">
            <fmt:message key="text.summaryOf"/>
            <c:out value="${userName}"/>
        </div>
        <c:choose>
            <c:when test="${empty summaries}">
                <fmt:message key="msg.warning.empty"/>
            </c:when>
            <c:otherwise>
                <table class="c-table">
                    <tr>
                        <th><fmt:message key="label.eventType"/></th>
                        <th><fmt:message key="label.eventCount"/></th>
                        <th><fmt:message key="label.avgTime"/></th>
                    </tr>
                    <c:forEach var="summary" items="${summaries}">
                        <tr>
                            <th><c:out value="${summary.eventType.name}"/></th>
                            <th><c:out value="${summary.eventCount}"/></th>
                            <th>
                                <fmt:formatNumber
                                        value="${summary.avgTimeInHour}"
                                        maxFractionDigits="3"
                                        minFractionDigits="3"
                                        minIntegerDigits="1"/>
                            </th>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>