<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <fmt:message key="title.err"/>
    </title>
    <link rel="stylesheet" href="<c:url value="/webjars/bootstrap/5.1.1/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="assets/css/custom-1.0.0.css"/>">
</head>
<body class="bg-secondary text-white">
<div class="d-flex w-100 justify-content-center h-100 align-items-center bg-white">
    <div>
        <c:choose>
            <c:when test="${not empty warningMsg}">
                <div class="text-warning">
                    <c:out value="${warningMsg}"/>
                </div>
            </c:when>
            <c:when test="${not empty successMsg}">
                <div class="text-success">
                    <c:out value="${successMsg}"/>
                </div>
            </c:when>
            <c:when test="${not empty errorMsg}">
                <div class="text-danger">
                    <c:out value="${errorMsg}"/>
                </div>
            </c:when>
            <c:otherwise>
                <fmt:message key="err.whiteLevel"/>
            </c:otherwise>
        </c:choose>
        <a href="<c:url value="${'/home'}"/>">
            <fmt:message key="msg.info.return.home"/>
        </a>
    </div>
</div>
</body>
</html>