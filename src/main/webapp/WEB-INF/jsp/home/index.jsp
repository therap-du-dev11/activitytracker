<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cstm" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <title>
        <fmt:message key="title.welcome"/>
    </title>
</head>
<body>
<div class="w-100 h-100 d-flex justify-content-center align-items-center">
    <h1><fmt:message key="msg.welcome"/></h1>
</div>
</body>
</html>