package net.therap.activitytracker.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.util.Objects.nonNull;

/**
 * @author iftakhar.ahmed
 * @since 1/16/22
 */
public class DateUtil {

    public static String dateToString(Date date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String strDate = null;
        if (nonNull(date)) {
            strDate = format.format(date);
        }

        return strDate;
    }
}