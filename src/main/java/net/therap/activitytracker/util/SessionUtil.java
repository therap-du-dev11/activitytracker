package net.therap.activitytracker.util;

import net.therap.activitytracker.domain.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

import static net.therap.activitytracker.constant.GlobalConstants.CURRENT_USER;

/**
 * @author iftakhar.ahmed
 * @since 12/30/21
 */
public class SessionUtil {

    public static HttpSession getSession() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attributes.getRequest().getSession();
    }

    public static Object getAttribute(String key) {
        return getSession().getAttribute(key);
    }

    public static User getUser() {
        return (User) getAttribute(CURRENT_USER);
    }

    public static void setAttribute(String key, Object value) {
        getSession().setAttribute(key, value);
    }

    public static boolean isCurrentUserId(long id) {
        return id == getUser().getId();
    }
}