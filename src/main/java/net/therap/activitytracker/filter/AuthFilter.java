package net.therap.activitytracker.filter;

import net.therap.activitytracker.domain.User;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

import static java.util.Objects.nonNull;
import static net.therap.activitytracker.constant.GlobalConstants.CURRENT_USER;

/**
 * @author iftakhar.ahmed
 * @since 1/10/22
 */
public class AuthFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute(CURRENT_USER);

        if (nonNull(user)) {
            chain.doFilter(request, response);
        } else {
            response.sendRedirect("/auth/login");
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return Set.of("/", "/home", "/auth/login").contains(request.getRequestURI())
                || request.getRequestURI().startsWith("/WEB-INF")
                || request.getRequestURI().startsWith("/assets")
                || request.getRequestURI().startsWith("/webjars")
                || request.getRequestURI().equals("/favicon.ico");
    }
}