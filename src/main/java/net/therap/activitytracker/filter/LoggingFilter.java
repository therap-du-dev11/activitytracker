package net.therap.activitytracker.filter;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.util.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Objects.isNull;

/**
 * @author iftakhar.ahmed
 * @since 1/12/22
 */
public class LoggingFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(LoggingFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {

        User user = SessionUtil.getUser();

        logger.info("[Type=PATH_TRACK, requested by user = {}, on path = {}]"
                , (isNull(user) ? "Unknown" : user.getEmail()), request.getRequestURI());

        chain.doFilter(request, response);
    }
}