package net.therap.activitytracker.helper;

import net.therap.activitytracker.command.UserCommand;
import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.exception.ResourceNotFoundException;
import net.therap.activitytracker.service.RoleService;
import net.therap.activitytracker.service.UserService;
import net.therap.activitytracker.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;
import static net.therap.activitytracker.constant.GlobalConstants.PAGE_SIZE;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
@Component
public class UserHelper {

    public static final String COMMAND = "userCommand";

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AccessManager accessManager;

    @Autowired
    private MessageSourceAccessor msa;

    public void setupRoles(ModelMap model) {
        model.addAttribute("roles", roleService.findAll());
    }

    public void setupSelfStatus(ModelMap model, User user) {
        model.addAttribute("self", SessionUtil.isCurrentUserId(user.getId()));
    }

    public void setupCommandAndSelfStatus(ModelMap model, long id) {
        User user = id == 0 ? new User() : userService.find(id);
        if (isNull(user)) {
            throw new ResourceNotFoundException(msa.getMessage("msg.err.resource.notFound"));
        }

        UserCommand command = new UserCommand(user, id != 0);

        model.addAttribute(COMMAND, command);
        setupSelfStatus(model, user);
    }

    public void setupReferenceDataForUsers(ModelMap model, String name, int pageNo) {
        if (isNull(name)) {
            name = "";
        }
        List<User> users = userService.findByName(name, pageNo);

        model.addAttribute("users", users);
        model.addAttribute("searchString", name);
        model.addAttribute("pageNo", pageNo);
        model.addAttribute("pageNoToView", pageNo + 1);
        model.addAttribute("hasPrev", pageNo > 0);
        model.addAttribute("hasNext", users.size() == PAGE_SIZE);
    }

    public String[] getAllowedFields(User user) {
        List<String> allowedFieldList = new ArrayList<>();
        allowedFieldList.add("user.name");
        allowedFieldList.add("user.email");
        allowedFieldList.add("user.phone");
        if (accessManager.isAdmin()) {
            allowedFieldList.add("user.roles");
        }

        if (user.isNew()) {
            allowedFieldList.add("user.password");
        }

        return allowedFieldList.toArray(new String[allowedFieldList.size()]);
    }
}