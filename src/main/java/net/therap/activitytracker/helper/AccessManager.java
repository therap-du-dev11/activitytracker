package net.therap.activitytracker.helper;

import net.therap.activitytracker.domain.Activity;
import net.therap.activitytracker.domain.Role;
import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.exception.AccessDeniedException;
import net.therap.activitytracker.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@Service
public class AccessManager {

    @Autowired
    private MessageSourceAccessor msa;

    public void checkAccess(Role... roles) {
        User user = SessionUtil.getUser();
        if (isNull(user)) {
            throw new AccessDeniedException(msa.getMessage("msg.err.access"));
        }

        for (Role role : roles) {
            if (user.getRoles().contains(role)) {
                return;
            }
        }

        throw new AccessDeniedException(msa.getMessage("msg.err.access"));
    }

    public void checkAccess(Activity activity, Role... roles) {
        if (isNull(activity) || isNull(activity.getUser())) {
            throw new AccessDeniedException(msa.getMessage("msg.err.access"));
        }

        checkAccess(activity.getUser().getId(), roles);
    }

    public void checkAccess(long id, Role... roles) {
        if (id == SessionUtil.getUser().getId()) {
            return;
        }

        checkAccess(roles);
    }

    public boolean isAdmin() {
        return SessionUtil.getUser().getRoles().contains(Role.ADMIN);
    }
}