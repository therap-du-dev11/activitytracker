package net.therap.activitytracker.helper;

import net.therap.activitytracker.command.PasswordCommand;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
@Component
public class PasswordHelper {

    private static final String PASSWORD_COMMAND = "passwordCommand";

    public void setupCommand(ModelMap model) {
        model.addAttribute(PASSWORD_COMMAND, new PasswordCommand());
    }
}