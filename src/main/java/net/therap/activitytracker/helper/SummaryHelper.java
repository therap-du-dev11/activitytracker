package net.therap.activitytracker.helper;

import net.therap.activitytracker.dto.ShortSummaryDto;
import net.therap.activitytracker.dto.SummaryDto;
import net.therap.activitytracker.service.SummaryService;
import net.therap.activitytracker.service.UserService;
import net.therap.activitytracker.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Objects.nonNull;
import static net.therap.activitytracker.constant.GlobalConstants.PAGE_SIZE;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
@Component
public class SummaryHelper {

    @Autowired
    private SummaryService summaryService;

    @Autowired
    private UserService userService;

    public void setupReferenceDataForAUserActivitySummary(ModelMap model, long id, Date startTime, Date endTime) {
        List<SummaryDto> summaries = new ArrayList<>();
        if (nonNull(startTime) && nonNull(endTime)) {
            if (startTime.before(endTime)) {
                summaries = summaryService.findByUserIdAndDuration(id, startTime, endTime);
            }
        } else {
            summaries = summaryService.findByUserId(id);
        }

        model.addAttribute("userId", id);
        model.addAttribute("startTime", DateUtil.dateToString(startTime));
        model.addAttribute("endTime", DateUtil.dateToString(endTime));
        model.addAttribute("summaries", summaries);
        model.addAttribute("userName", userService.find(id).getEmail());
    }

    public void setupReferenceDataForAllUserActivitySummary(ModelMap model, Date startTime, Date endTime, int pageNo) {
        List<ShortSummaryDto> summaries = new ArrayList<>();
        if (nonNull(startTime) && nonNull(endTime)) {
            if (startTime.before(endTime)) {
                summaries = summaryService.findByDuration(startTime, endTime, pageNo);
            }
        } else {
            summaries = summaryService.findAll(pageNo);
        }

        model.addAttribute("summaries", summaries);
        model.addAttribute("startTime", DateUtil.dateToString(startTime));
        model.addAttribute("endTime", DateUtil.dateToString(endTime));
        model.addAttribute("pageNo", pageNo);
        model.addAttribute("pageNoToView", pageNo + 1);
        model.addAttribute("hasPrev", pageNo > 0);
        model.addAttribute("hasNext", summaries.size() == PAGE_SIZE);
    }
}