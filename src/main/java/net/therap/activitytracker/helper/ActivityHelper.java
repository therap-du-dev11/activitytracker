package net.therap.activitytracker.helper;

import net.therap.activitytracker.command.ActivityCommand;
import net.therap.activitytracker.domain.Activity;
import net.therap.activitytracker.exception.ResourceNotFoundException;
import net.therap.activitytracker.service.ActivityService;
import net.therap.activitytracker.service.EventTypeService;
import net.therap.activitytracker.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.util.List;

import static java.util.Objects.isNull;
import static net.therap.activitytracker.constant.GlobalConstants.*;
import static net.therap.activitytracker.domain.Role.ADMIN;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
@Component
public class ActivityHelper {

    private static final String COMMAND = "activityCommand";

    @Autowired
    private ActivityService activityService;

    @Autowired
    private EventTypeService eventTypeService;

    @Autowired
    private MessageSourceAccessor msa;

    public void setupEventTypes(ModelMap model) {
        model.addAttribute("eventTypes", eventTypeService.findAll());
    }

    public void setupCommand(ModelMap model, long id) {

        Activity activity = id == 0 ? new Activity(SessionUtil.getUser()) : activityService.find(id);
        if (isNull(activity)) {
            throw new ResourceNotFoundException(msa.getMessage("msg.err.resource.notFound"));
        }

        model.addAttribute(COMMAND, new ActivityCommand(activity, id != 0));
    }

    public void setupSearchResultActivities(ModelMap model, String email, int pageNo) {
        List<Activity> activities;
        if (SessionUtil.getUser().getRoles().contains(ADMIN)) {
            activities = (isNull(email) || email.isEmpty())
                    ? activityService.findAll(pageNo)
                    : activityService.findByEmail(email, pageNo);
        } else {
            activities = activityService.findByEmail(SessionUtil.getUser().getEmail(), pageNo);
        }

        model.addAttribute("activities", activities);
        model.addAttribute("email", email);
        model.addAttribute("pageNo", pageNo);
        model.addAttribute("pageNoToView", pageNo + 1);
        model.addAttribute("hasPrev", pageNo > 0);
        model.addAttribute("hasNext", activities.size() == PAGE_SIZE);
    }
}