package net.therap.activitytracker.helper;

import net.therap.activitytracker.util.SessionUtil;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import static java.util.Objects.nonNull;
import static net.therap.activitytracker.constant.GlobalConstants.IS_ADMIN;
import static net.therap.activitytracker.constant.GlobalConstants.IS_USER;
import static net.therap.activitytracker.domain.Role.ADMIN;
import static net.therap.activitytracker.domain.Role.USER;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
@Component
public class ControllerHelper {

    public void setupAdminAndUserStatus(ModelMap model) {
        if (nonNull(SessionUtil.getUser())) {
            model.addAttribute(IS_ADMIN, SessionUtil.getUser().getRoles().contains(ADMIN));
            model.addAttribute(IS_USER, SessionUtil.getUser().getRoles().contains(USER));
        }
    }
}