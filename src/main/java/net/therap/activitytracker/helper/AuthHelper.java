package net.therap.activitytracker.helper;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.command.LoginCommand;
import net.therap.activitytracker.util.SessionUtil;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import static net.therap.activitytracker.constant.GlobalConstants.*;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
@Component
public class AuthHelper {

    public static final String LOGIN_COMMAND = "loginCommand";

    public void setupCommand(ModelMap model, LoginCommand loginCommand) {
        model.addAttribute(LOGIN_COMMAND, loginCommand);
    }

    public void setupErrorMessage(ModelMap model, String errorMsg) {
        model.addAttribute(ERROR_MESSAGE, errorMsg);
    }

    public void setUserToSession(User user) {
        SessionUtil.setAttribute(CURRENT_USER, user);
    }
}