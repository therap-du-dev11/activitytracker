package net.therap.activitytracker.helper;

import net.therap.activitytracker.command.EventTypeCommand;
import net.therap.activitytracker.domain.EventType;
import net.therap.activitytracker.exception.ResourceNotFoundException;
import net.therap.activitytracker.service.EventTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import static java.util.Objects.isNull;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
@Component
public class EventTypeHelper {

    private static final String COMMAND = "eventTypeCommand";

    @Autowired
    private EventTypeService eventTypeService;

    @Autowired
    private MessageSourceAccessor msa;


    public void setupCommand(ModelMap model, long id) {
        EventType eventType = id == 0 ? new EventType() : eventTypeService.find(id);
        if (isNull(eventType)) {
            throw new ResourceNotFoundException(msa.getMessage("msg.err.resource.notFound"));
        }

        model.addAttribute(COMMAND, new EventTypeCommand(eventType, id != 0));
    }

    public void setupEventTypes(ModelMap model) {
        model.addAttribute("eventTypes", eventTypeService.findAll());
    }
}
