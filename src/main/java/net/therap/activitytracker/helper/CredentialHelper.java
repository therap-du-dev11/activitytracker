package net.therap.activitytracker.helper;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.command.LoginCommand;
import net.therap.activitytracker.service.UserService;
import net.therap.activitytracker.util.HashGenerationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.util.Objects.nonNull;

/**
 * @author iftakhar.ahmed
 * @since 1/8/22
 */
@Component
public class CredentialHelper {

    @Autowired
    private UserService userService;

    public boolean isValid(LoginCommand loginCommand) {
        User user = userService.findByEmail(loginCommand.getEmail());
        String newPasswordHash = HashGenerationUtil.getMd5(loginCommand.getPassword());

        return nonNull(user) && user.getPassword().equals(newPasswordHash);
    }
}