package net.therap.activitytracker.formatter;

import net.therap.activitytracker.domain.EventType;
import org.springframework.format.Formatter;

import java.util.Locale;

/**
 * @author iftakhar.ahmed
 * @since 1/19/22
 */
public class EventTypeFormatter implements Formatter<EventType> {

    @Override
    public EventType parse(String text, Locale locale) {
        try {
            EventType eventType = new EventType();
            eventType.setId(Long.parseLong(text));
            return eventType;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String print(EventType object, Locale locale) {
        return String.valueOf(object.getId());
    }
}
