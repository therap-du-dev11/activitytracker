package net.therap.activitytracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author iftakhar.ahmed
 * @since 1/16/22
 */
@SpringBootApplication
public class ActivitytrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivitytrackerApplication.class, args);
    }
}