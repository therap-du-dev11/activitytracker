package net.therap.activitytracker.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author iftakhar.ahmed
 * @since 1/12/22
 */
@Setter
@Getter
@AllArgsConstructor
public class ShortSummaryDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private long userId;

    private String UserEmail;

    private long eventCount;

    private double avgEventTimeInHour;
}
