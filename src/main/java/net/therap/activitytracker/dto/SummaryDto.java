package net.therap.activitytracker.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.therap.activitytracker.domain.EventType;

import java.io.Serializable;

/**
 * @author iftakhar.ahmed
 * @since 1/12/22
 */
@Setter
@Getter
@AllArgsConstructor
public class SummaryDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private EventType eventType;

    private long eventCount;

    private double avgTimeInHour;
}