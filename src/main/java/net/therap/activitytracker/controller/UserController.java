package net.therap.activitytracker.controller;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.exception.InvalidStateException;
import net.therap.activitytracker.helper.AccessManager;
import net.therap.activitytracker.helper.ControllerHelper;
import net.therap.activitytracker.helper.UserHelper;
import net.therap.activitytracker.service.UserService;
import net.therap.activitytracker.validator.UserValidator;
import net.therap.activitytracker.command.UserCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static net.therap.activitytracker.constant.GlobalConstants.*;
import static net.therap.activitytracker.domain.Role.ADMIN;

/**
 * @author iftakhar.ahmed
 * @since 1/8/22
 */
@Controller
@RequestMapping("/user")
@SessionAttributes(UserController.COMMAND)
public class UserController {

    private static final String USER_FORM = "user/user";
    private static final String USER_LIST = "user/userList";
    private static final String REDIRECT_TO_USER_LIST = "redirect:/user/list";
    public static final String COMMAND = "userCommand";

    private final Logger logger;

    @Autowired
    private UserService userService;

    @Autowired
    private AccessManager accessManager;

    @Autowired
    private UserHelper userHelper;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private MessageSourceAccessor msa;

    public UserController() {
        this.logger = LoggerFactory.getLogger(UserController.class);
    }

    @InitBinder(COMMAND)
    public void userBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.addValidators(userValidator);
        UserCommand command = (UserCommand) binder.getTarget();
        if (command.isReadOnly()) {
            binder.setDisallowedFields("*");
        } else {
            String[] allowedFields = userHelper.getAllowedFields(command.getUser());
            binder.setAllowedFields(allowedFields);
        }
    }

    @GetMapping("/action")
    public String getForm(@RequestParam(defaultValue = "0") long id, ModelMap model) {
        accessManager.checkAccess(id, ADMIN);

        userHelper.setupCommandAndSelfStatus(model, id);
        userHelper.setupRoles(model);
        controllerHelper.setupAdminAndUserStatus(model);

        return USER_FORM;
    }

    @GetMapping(value = "/action", params = "_action_edit")
    public String edit(@ModelAttribute(COMMAND) UserCommand command,
                       ModelMap model) {

        accessManager.checkAccess(command.getUser().getId(), ADMIN);

        command.setReadOnly(false);

        userHelper.setupRoles(model);
        userHelper.setupSelfStatus(model, command.getUser());
        controllerHelper.setupAdminAndUserStatus(model);

        return USER_FORM;
    }

    @PostMapping(value = "/action", params = "_action_saveOrUpdate")
    public String saveOrUpdate(@Valid @ModelAttribute(COMMAND) UserCommand command,
                               BindingResult bindingResult,
                               ModelMap model,
                               SessionStatus sessionStatus,
                               RedirectAttributes ra) {

        User user = command.getUser();
        accessManager.checkAccess(user.getId(), ADMIN);

        if (bindingResult.hasErrors()) {
            userHelper.setupRoles(model);
            controllerHelper.setupAdminAndUserStatus(model);

            return USER_FORM;
        }

        logger.info(CRUD_LOG_FORMAT, user.isNew() ? SAVE : UPDATE, COMMAND, user.getId());

        userService.saveOrUpdate(user);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.action.success"));

        return REDIRECT_TO_USER_LIST;
    }

    @PostMapping(value = "/action", params = "_action_delete")
    public String delete(@Valid @ModelAttribute(COMMAND) UserCommand command,
                         BindingResult bindingResult,
                         ModelMap model,
                         SessionStatus sessionStatus,
                         RedirectAttributes ra) {

        accessManager.checkAccess(ADMIN);

        if (!command.isReadOnly()) {
            throw new InvalidStateException(msa.getMessage("msg.err.invalidState"));
        }

        if (bindingResult.hasErrors()) {
            userHelper.setupRoles(model);
            controllerHelper.setupAdminAndUserStatus(model);

            return USER_FORM;
        }

        User user = command.getUser();
        logger.info(CRUD_LOG_FORMAT, DELETE, COMMAND, user.getId());

        userService.delete(user);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.success.user.deleted"));

        return REDIRECT_TO_USER_LIST;
    }

    @GetMapping("/list")
    public String findAll(@RequestParam(name = "searchString", required = false) String name,
                          @RequestParam(defaultValue = "0") int pageNo,
                          ModelMap model) {

        accessManager.checkAccess(ADMIN);

        userHelper.setupReferenceDataForUsers(model, name, pageNo);
        controllerHelper.setupAdminAndUserStatus(model);

        return USER_LIST;
    }
}