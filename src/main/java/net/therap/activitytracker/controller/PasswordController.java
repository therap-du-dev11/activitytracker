package net.therap.activitytracker.controller;

import net.therap.activitytracker.constant.GlobalConstants;
import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.command.PasswordCommand;
import net.therap.activitytracker.helper.AccessManager;
import net.therap.activitytracker.helper.ControllerHelper;
import net.therap.activitytracker.helper.PasswordHelper;
import net.therap.activitytracker.service.UserService;
import net.therap.activitytracker.util.SessionUtil;
import net.therap.activitytracker.validator.PasswordValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static net.therap.activitytracker.constant.GlobalConstants.*;
import static net.therap.activitytracker.domain.Role.ADMIN;
import static net.therap.activitytracker.domain.Role.USER;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@Controller
@RequestMapping("/password")
@SessionAttributes(PasswordController.PASSWORD_COMMAND)
public class PasswordController {

    private static final String PASSWORD_CHANGE_FORM = "user/password";
    private static final String REDIRECT_TO_HOME = "redirect:/home";
    public static final String PASSWORD_COMMAND = "passwordCommand";

    private final Logger logger;

    @Autowired
    private UserService userService;

    @Autowired
    private AccessManager accessManager;

    @Autowired
    private PasswordHelper passwordHelper;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private PasswordValidator passwordValidator;

    @Autowired
    private MessageSourceAccessor msa;

    public PasswordController() {
        this.logger = LoggerFactory.getLogger(PasswordController.class);
    }

    @InitBinder(PASSWORD_COMMAND)
    public void passwordDtoBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.addValidators(passwordValidator);
        binder.setAllowedFields("oldPassword", "newPassword", "confirmPassword");
    }

    @GetMapping("/change")
    public String getForm(ModelMap model) {
        accessManager.checkAccess(ADMIN, USER);

        passwordHelper.setupCommand(model);
        controllerHelper.setupAdminAndUserStatus(model);

        return PASSWORD_CHANGE_FORM;
    }

    @PostMapping("/change")
    public String change(@Valid @ModelAttribute(PASSWORD_COMMAND) PasswordCommand passwordCommand,
                         BindingResult bindingResult,
                         ModelMap model,
                         SessionStatus sessionStatus,
                         RedirectAttributes ra) {

        accessManager.checkAccess(ADMIN, USER);

        if (bindingResult.hasErrors()) {
            controllerHelper.setupAdminAndUserStatus(model);

            return PASSWORD_CHANGE_FORM;
        }

        User user = SessionUtil.getUser();
        logger.info(CRUD_LOG_FORMAT, "PASSWORD_CHANGE", "user", user.getId());

        user = userService.updatePassword(user, passwordCommand.getNewPassword());
        SessionUtil.setAttribute(GlobalConstants.CURRENT_USER, user);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.success.passwordChange"));

        return REDIRECT_TO_HOME;
    }
}