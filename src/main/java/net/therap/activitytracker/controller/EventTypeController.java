package net.therap.activitytracker.controller;

import net.therap.activitytracker.domain.EventType;
import net.therap.activitytracker.exception.InvalidStateException;
import net.therap.activitytracker.helper.AccessManager;
import net.therap.activitytracker.helper.ControllerHelper;
import net.therap.activitytracker.helper.EventTypeHelper;
import net.therap.activitytracker.service.EventTypeService;
import net.therap.activitytracker.validator.EventTypeValidator;
import net.therap.activitytracker.command.EventTypeCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static net.therap.activitytracker.constant.GlobalConstants.*;
import static net.therap.activitytracker.domain.Role.ADMIN;
import static net.therap.activitytracker.domain.Role.USER;

/**
 * @author iftakhar.ahmed
 * @since 1/10/22
 */
@Controller
@RequestMapping("/eventType")
@SessionAttributes(EventTypeController.COMMAND)
public class EventTypeController {

    private static final String EVENT_TYPE_FORM = "event/eventType";
    private static final String EVENT_TYPE_LIST = "event/eventTypeList";
    private static final String REDIRECT_TO_LIST_OF_EVENT_TYPE = "redirect:/eventType/list";
    public static final String COMMAND = "eventTypeCommand";

    private final Logger logger;

    @Autowired
    private EventTypeService eventTypeService;

    @Autowired
    private AccessManager accessManager;

    @Autowired
    private EventTypeHelper eventTypeHelper;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private EventTypeValidator eventTypeValidator;

    @Autowired
    private MessageSourceAccessor msa;

    public EventTypeController() {
        this.logger = LoggerFactory.getLogger(EventTypeController.class);
    }

    @InitBinder(COMMAND)
    public void eventTypeBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.addValidators(eventTypeValidator);
        EventTypeCommand command = (EventTypeCommand) binder.getTarget();
        if (command.isReadOnly()) {
            binder.setDisallowedFields("*");
        } else {
            binder.setAllowedFields("eventType.name", "eventType.description");
        }
    }

    @GetMapping("/action")
    public String getForm(@RequestParam(defaultValue = "0") long id, ModelMap model) {
        accessManager.checkAccess(ADMIN, USER);

        eventTypeHelper.setupCommand(model, id);
        controllerHelper.setupAdminAndUserStatus(model);

        return EVENT_TYPE_FORM;
    }

    @GetMapping(value = "/action", params = "_action_edit")
    public String edit(@ModelAttribute(COMMAND) EventTypeCommand command,
                       ModelMap model) {

        accessManager.checkAccess(ADMIN);

        command.setReadOnly(false);
        controllerHelper.setupAdminAndUserStatus(model);

        return EVENT_TYPE_FORM;
    }

    @PostMapping(value = "/action", params = "_action_saveOrUpdate")
    public String saveOrUpdate(@Valid @ModelAttribute(COMMAND) EventTypeCommand command,
                               BindingResult bindingResult,
                               ModelMap model,
                               SessionStatus sessionStatus,
                               RedirectAttributes ra) {

        accessManager.checkAccess(ADMIN);

        if (bindingResult.hasErrors()) {
            controllerHelper.setupAdminAndUserStatus(model);

            return EVENT_TYPE_FORM;
        }

        EventType eventType = command.getEventType();
        logger.info(CRUD_LOG_FORMAT, eventType.isNew() ? SAVE : UPDATE, COMMAND, eventType.getId());

        eventTypeService.saveOrUpdate(eventType);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.action.success"));

        return REDIRECT_TO_LIST_OF_EVENT_TYPE;
    }

    @PostMapping(value = "/action", params = "_action_delete")
    public String delete(@Valid @ModelAttribute(COMMAND) EventTypeCommand command,
                         BindingResult bindingResult,
                         ModelMap model,
                         SessionStatus sessionStatus,
                         RedirectAttributes ra) {

        accessManager.checkAccess(ADMIN);

        EventType eventType = command.getEventType();

        if (!command.isReadOnly()) {
            throw new InvalidStateException(msa.getMessage("msg.err.invalidState"));
        }

        if (bindingResult.hasErrors()) {
            controllerHelper.setupAdminAndUserStatus(model);

            return EVENT_TYPE_FORM;
        }

        logger.info(CRUD_LOG_FORMAT, DELETE, COMMAND, eventType.getId());

        eventTypeService.delete(eventType);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.success.eventType.deleted"));

        return REDIRECT_TO_LIST_OF_EVENT_TYPE;
    }

    @GetMapping("/list")
    public String findAll(ModelMap model) {
        eventTypeHelper.setupEventTypes(model);
        controllerHelper.setupAdminAndUserStatus(model);

        return EVENT_TYPE_LIST;
    }
}