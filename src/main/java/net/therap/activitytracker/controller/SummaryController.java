package net.therap.activitytracker.controller;

import net.therap.activitytracker.helper.AccessManager;
import net.therap.activitytracker.helper.ControllerHelper;
import net.therap.activitytracker.helper.SummaryHelper;
import net.therap.activitytracker.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

import static net.therap.activitytracker.domain.Role.ADMIN;

/**
 * @author iftakhar.ahmed
 * @since 1/12/22
 */
@Controller
@RequestMapping("/summary")
public class SummaryController {

    private static final String SUMMARY_PAGE = "summary/summary";
    private static final String SUMMARY_LIST = "summary/summaryList";

    @Autowired
    private SummaryHelper summaryHelper;

    @Autowired
    private AccessManager accessManager;

    @Autowired
    private ControllerHelper controllerHelper;

    @GetMapping("/list")
    public String findAll(@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date startTime,
                          @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date endTime,
                          @RequestParam(defaultValue = "0") int pageNo,
                          ModelMap model) {

        accessManager.checkAccess(ADMIN);

        summaryHelper.setupReferenceDataForAllUserActivitySummary(model, startTime, endTime, pageNo);
        controllerHelper.setupAdminAndUserStatus(model);

        return SUMMARY_LIST;
    }

    @GetMapping("/view")
    public String view(@RequestParam(name = "userId", defaultValue = "0") long userId,
                       @RequestParam(name = "startTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date startTime,
                       @RequestParam(name = "endTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date endTime,
                       ModelMap model) {

        if (userId == 0) {
            userId = SessionUtil.getUser().getId();
        }

        accessManager.checkAccess(userId, ADMIN);

        summaryHelper.setupReferenceDataForAUserActivitySummary(model, userId, startTime, endTime);
        controllerHelper.setupAdminAndUserStatus(model);

        return SUMMARY_PAGE;
    }
}