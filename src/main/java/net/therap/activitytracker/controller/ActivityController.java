package net.therap.activitytracker.controller;

import net.therap.activitytracker.domain.Activity;
import net.therap.activitytracker.domain.EventType;
import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.exception.InvalidStateException;
import net.therap.activitytracker.helper.ActivityHelper;
import net.therap.activitytracker.helper.ControllerHelper;
import net.therap.activitytracker.formatter.EventTypeFormatter;
import net.therap.activitytracker.propertyEditor.UserEditor;
import net.therap.activitytracker.helper.AccessManager;
import net.therap.activitytracker.service.ActivityService;
import net.therap.activitytracker.validator.ActivityValidator;
import net.therap.activitytracker.command.ActivityCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import java.text.SimpleDateFormat;
import java.util.Date;

import static net.therap.activitytracker.constant.GlobalConstants.*;
import static net.therap.activitytracker.domain.Role.ADMIN;
import static net.therap.activitytracker.domain.Role.USER;

/**
 * @author iftakhar.ahmed
 * @since 1/11/22
 */
@Controller
@RequestMapping("/activity")
@SessionAttributes(ActivityController.COMMAND)
public class ActivityController {

    private static final String ACTIVITY_FORM = "activity/activity";
    private static final String ACTIVITY_LIST = "activity/activityList";
    private static final String REDIRECT_TO_ACTIVITY_LIST = "redirect:/activity/list";
    public static final String COMMAND = "activityCommand";

    private final Logger logger;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private AccessManager accessManager;

    @Autowired
    private ActivityHelper activityHelper;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private UserEditor userEditor;

    @Autowired
    private ActivityValidator activityValidator;

    @Autowired
    private MessageSourceAccessor msa;

    public ActivityController() {
        this.logger = LoggerFactory.getLogger(ActivityController.class);
    }

    @InitBinder(COMMAND)
    public void activityBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(User.class, userEditor);
        binder.addCustomFormatter(new EventTypeFormatter(), EventType.class);
        binder.registerCustomEditor(Date.class
                , new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm"), true));
        binder.addValidators(activityValidator);
        ActivityCommand activityCommand = (ActivityCommand) binder.getTarget();
        if (activityCommand.isReadOnly()) {
            binder.setDisallowedFields("*");
        } else {
            binder.setAllowedFields("activity.startTime", "activity.endTime"
                    , "activity.note", "activity.eventType", "activity.relatedPersons");
        }
    }

    @GetMapping("/action")
    public String getForm(@RequestParam(defaultValue = "0") long id,
                          ModelMap model) {

        if (0 == id) {
            accessManager.checkAccess(USER, ADMIN);
        } else {
            accessManager.checkAccess(activityService.find(id));
        }

        activityHelper.setupCommand(model, id);
        activityHelper.setupEventTypes(model);
        controllerHelper.setupAdminAndUserStatus(model);

        return ACTIVITY_FORM;
    }

    @GetMapping(value = "/action", params = "_action_edit")
    public String edit(@ModelAttribute(COMMAND) ActivityCommand command,
                       ModelMap model) {

        accessManager.checkAccess(command.getActivity());

        command.setReadOnly(false);
        activityHelper.setupEventTypes(model);
        controllerHelper.setupAdminAndUserStatus(model);

        return ACTIVITY_FORM;
    }

    @PostMapping(value = "/action", params = "_action_saveOrUpdate")
    public String saveOrUpdate(@Valid @ModelAttribute(COMMAND) ActivityCommand command,
                               BindingResult bindingResult,
                               ModelMap model,
                               SessionStatus sessionStatus,
                               RedirectAttributes ra) {

        Activity activity = command.getActivity();
        if (activity.isNew()) {
            accessManager.checkAccess(USER, ADMIN);
        } else {
            accessManager.checkAccess(activity);
        }

        if (bindingResult.hasErrors()) {
            activityHelper.setupEventTypes(model);
            controllerHelper.setupAdminAndUserStatus(model);

            return ACTIVITY_FORM;
        }

        logger.info(CRUD_LOG_FORMAT, activity.isNew() ? SAVE : UPDATE , COMMAND, activity.getId());

        activityService.saveOrUpdate(activity);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.action.success"));

        return REDIRECT_TO_ACTIVITY_LIST;
    }

    @PostMapping(value = "/action", params = "_action_delete")
    public String delete(@Valid @ModelAttribute(COMMAND) ActivityCommand command,
                         BindingResult bindingResult,
                         ModelMap model,
                         SessionStatus sessionStatus,
                         RedirectAttributes ra) {

        Activity activity = command.getActivity();
        accessManager.checkAccess(activity, ADMIN);

        if (!command.isReadOnly()) {
            throw new InvalidStateException(msa.getMessage("msg.err.invalidState"));
        }

        if (bindingResult.hasErrors()) {
            activityHelper.setupEventTypes(model);
            controllerHelper.setupAdminAndUserStatus(model);

            return ACTIVITY_FORM;
        }

        logger.info(CRUD_LOG_FORMAT, DELETE, COMMAND, activity.getId());

        activityService.delete(activity);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.success.activity.deleted"));

        return REDIRECT_TO_ACTIVITY_LIST;
    }

    @GetMapping("/list")
    public String findAll(@RequestParam(required = false) String email,
                          @RequestParam(defaultValue = "0") int pageNo,
                          ModelMap model) {

        accessManager.checkAccess(ADMIN, USER);

        activityHelper.setupSearchResultActivities(model, email, pageNo);
        controllerHelper.setupAdminAndUserStatus(model);

        return ACTIVITY_LIST;
    }
}