package net.therap.activitytracker.controller;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.helper.AccessManager;
import net.therap.activitytracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.nonNull;
import static net.therap.activitytracker.domain.Role.ADMIN;
import static net.therap.activitytracker.domain.Role.USER;

/**
 * @author iftakhar.ahmed
 * @since 1/11/22
 */
@RestController
@RequestMapping("/rest/user")
public class RestUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private AccessManager accessManager;

    @GetMapping("/exists")
    public boolean exists(@RequestParam String email) {
        accessManager.checkAccess(ADMIN, USER);

        User user = userService.findByEmail(email);
        return nonNull(user);
    }
}