package net.therap.activitytracker.controller;

import net.therap.activitytracker.helper.ControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@Controller
public class IndexController {

    @Autowired
    private ControllerHelper controllerHelper;

    private static final String HOME_PAGE = "home/index";

    @GetMapping({"/", "/home"})
    public String home(ModelMap model) {
        controllerHelper.setupAdminAndUserStatus(model);

        return HOME_PAGE;
    }
}