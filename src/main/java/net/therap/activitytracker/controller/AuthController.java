package net.therap.activitytracker.controller;

import net.therap.activitytracker.command.LoginCommand;
import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.helper.AuthHelper;
import net.therap.activitytracker.helper.ControllerHelper;
import net.therap.activitytracker.helper.CredentialHelper;
import net.therap.activitytracker.service.UserService;
import net.therap.activitytracker.util.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static net.therap.activitytracker.constant.GlobalConstants.*;

/**
 * @author iftakhar.ahmed
 * @since 1/3/22
 */
@Controller
@RequestMapping("/auth")
@SessionAttributes(AuthController.LOGIN_COMMAND)
public class AuthController {

    private static final String LOGIN_FORM = "user/login";
    private static final String REDIRECT_TO_HOME = "redirect:/home";

    public static final String LOGIN_COMMAND = "loginCommand";

    private final Logger logger;

    @Autowired
    private UserService userService;

    @Autowired
    private CredentialHelper credentialHelper;

    @Autowired
    private AuthHelper authHelper;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private MessageSourceAccessor msa;

    public AuthController() {
        this.logger = LoggerFactory.getLogger(AuthController.class);
    }

    @InitBinder(LOGIN_COMMAND)
    public void loginDtoBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.setAllowedFields("email", "password");
    }

    @GetMapping("/login")
    public String getLoginForm(ModelMap model) {
        authHelper.setupCommand(model, new LoginCommand());
        controllerHelper.setupAdminAndUserStatus(model);

        return LOGIN_FORM;
    }

    @PostMapping("/login")
    public String login(@Valid @ModelAttribute(LOGIN_COMMAND) LoginCommand loginCommand,
                        BindingResult bindingResult,
                        ModelMap model,
                        SessionStatus sessionStatus,
                        RedirectAttributes ra) {

        if (bindingResult.hasErrors()) {
            controllerHelper.setupAdminAndUserStatus(model);

            return LOGIN_FORM;
        }

        if (!credentialHelper.isValid(loginCommand)) {
            authHelper.setupErrorMessage(model, msa.getMessage("msg.err.credential.invalid"));
            controllerHelper.setupAdminAndUserStatus(model);

            return LOGIN_FORM;
        }

        User user = userService.findByEmail(loginCommand.getEmail());
        logger.info(CRUD_LOG_FORMAT, LOGIN, null, user.getId());

        authHelper.setUserToSession(user);

        sessionStatus.setComplete();
        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.login.success"));

        return REDIRECT_TO_HOME;
    }

    @GetMapping("/logout")
    public String logout(HttpSession session,
                         SessionStatus sessionStatus,
                         HttpServletResponse response,
                         RedirectAttributes ra) {

        long id = SessionUtil.getUser().getId();
        sessionStatus.setComplete();
        session.invalidate();
        response.setHeader("Cache-Control", "no-cache, no-store," +
                " must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);

        logger.info(CRUD_LOG_FORMAT, LOGOUT, null, id);

        ra.addFlashAttribute(SUCCESS_REDIRECT_MESSAGE, msa.getMessage("msg.logout.success"));

        return REDIRECT_TO_HOME;
    }
}