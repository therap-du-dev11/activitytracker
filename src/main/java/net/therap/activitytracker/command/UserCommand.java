package net.therap.activitytracker.command;

import lombok.Getter;
import lombok.Setter;
import net.therap.activitytracker.domain.User;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * @author iftakhar.ahmed
 * @since 1/16/22
 */
@Setter
@Getter
public class UserCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private User user;

    private boolean readOnly;

    public UserCommand() {
        this.user = new User();
        this.readOnly = true;
    }

    public UserCommand(User user, boolean readOnly) {
        this.user = user;
        this.readOnly = readOnly;
    }
}