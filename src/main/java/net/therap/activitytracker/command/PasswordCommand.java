package net.therap.activitytracker.command;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@Setter
@Getter
public class PasswordCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 6, max = 255)
    private String oldPassword;

    @NotNull
    @Size(min = 6, max = 255)
    private String newPassword;

    @NotNull
    @Size(min = 6, max = 255)
    private String confirmPassword;
}