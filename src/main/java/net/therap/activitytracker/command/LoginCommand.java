package net.therap.activitytracker.command;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author iftakhar.ahmed
 * @since 1/8/22
 */
@Setter
@Getter
public class LoginCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String email;

    @NotNull
    @Size(max = 255)
    private String password;
}