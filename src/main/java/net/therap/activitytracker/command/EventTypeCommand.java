package net.therap.activitytracker.command;

import lombok.Getter;
import lombok.Setter;
import net.therap.activitytracker.domain.EventType;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * @author iftakhar.ahmed
 * @since 1/16/22
 */
@Getter
@Setter
public class EventTypeCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private EventType eventType;

    private boolean readOnly;

    public EventTypeCommand() {
        this.readOnly = true;
    }

    public EventTypeCommand(EventType eventType, boolean readOnly) {
        this.eventType = eventType;
        this.readOnly = readOnly;
    }
}