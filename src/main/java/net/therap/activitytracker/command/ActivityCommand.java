package net.therap.activitytracker.command;

import lombok.Getter;
import lombok.Setter;
import net.therap.activitytracker.domain.Activity;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * @author iftakhar.ahmed
 * @since 1/16/22
 */
@Setter
@Getter
public class ActivityCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private Activity activity;

    private boolean readOnly;

    public ActivityCommand() {
        this.readOnly = true;
    }

    public ActivityCommand(Activity activity, boolean readOnly) {
        this.activity = activity;
        this.readOnly = readOnly;
    }
}