package net.therap.activitytracker.service;

import net.therap.activitytracker.domain.Role;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@Service
public class RoleService {

    public List<Role> findAll() {
        return List.of(Role.values());
    }
}