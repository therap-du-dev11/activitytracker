package net.therap.activitytracker.service;

import net.therap.activitytracker.domain.EventType;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.List;

import static java.util.Objects.nonNull;

/**
 * @author iftakhar.ahmed
 * @since 1/10/22
 */
@Service
public class EventTypeService {

    @PersistenceContext
    private EntityManager em;

    public EventType find(long id) {
        return em.find(EventType.class, id);
    }

    @Transactional
    public EventType saveOrUpdate(EventType eventType) {
        if (eventType.isNew()) {
            em.persist(eventType);
        } else {
            eventType = em.merge(eventType);
        }

        return eventType;
    }

    @Transactional
    public void delete(EventType eventType) {
        em.remove(em.getReference(EventType.class, eventType.getId()));
    }

    public EventType findByName(String name) {
        EventType eventType;

        try {
            eventType = em.createNamedQuery("EventType.findByName", EventType.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            eventType = null;
        }

        return eventType;
    }

    public List<EventType> findAll() {
        return em.createNamedQuery("EventType.findAll", EventType.class)
                .getResultList();
    }

    public boolean exists(long id) {
        return nonNull(find(id));
    }
}