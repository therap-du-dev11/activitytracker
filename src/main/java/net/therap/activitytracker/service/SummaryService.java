package net.therap.activitytracker.service;

import net.therap.activitytracker.dto.ShortSummaryDto;
import net.therap.activitytracker.dto.SummaryDto;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

import static net.therap.activitytracker.constant.GlobalConstants.PAGE_SIZE;

/**
 * @author iftakhar.ahmed
 * @since 1/12/22
 */
@Service
public class SummaryService {

    @PersistenceContext
    private EntityManager em;

    private static final String FIND_BY_DURATION_QUERY = "SELECT " +
            "new net.therap.activitytracker.dto.ShortSummaryDto(a.user.id, a.user.email, count (a), AVG (CASE " +
            "WHEN (a.startTime > :startTime AND a.endTime < :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',a.endTime , a.startTime))/3600) " +
            "WHEN (a.startTime < :startTime AND a.endTime > :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',:endTime , :startTime))/3600) " +
            "WHEN (a.startTime < :startTime AND a.endTime < :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',a.endTime , :startTime))/3600) " +
            "WHEN (a.startTime > :startTime AND a.endTime > :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',:endTime , a.startTime))/3600) " +
            "ELSE 0 END))" +
            "FROM Activity a " +
            "WHERE (a.endTime > :startTime AND a.startTime < :endTime)" +
            "GROUP BY a.user " +
            "ORDER BY a.user.name";

    private static final String FIND_ALL_QUERY = "SELECT " +
            "new net.therap.activitytracker.dto.ShortSummaryDto(a.user.id, a.user.email, count (a)" +
            ", AVG (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',a.endTime , a.startTime)))/3600) " +
            "FROM Activity a " +
            "GROUP BY a.user " +
            "ORDER BY a.user.name";

    private static final String FIND_BY_USER_AND_DURATION_QUERY = "SELECT " +
            "new net.therap.activitytracker.dto.SummaryDto(a.eventType, COUNT (a), AVG (CASE " +
            "WHEN (a.startTime > :startTime AND a.endTime < :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',a.endTime , a.startTime))/3600) " +
            "WHEN (a.startTime < :startTime AND a.endTime > :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',:endTime , :startTime))/3600)" +
            "WHEN (a.startTime < :startTime AND a.endTime < :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',a.endTime , :startTime))/3600)" +
            "WHEN (a.startTime > :startTime AND a.endTime > :endTime) " +
            "THEN (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',:endTime , a.startTime))/3600)" +
            "ELSE 0 END))" +
            "FROM Activity a " +
            "WHERE (a.user.id = :userId) AND (a.endTime > :startTime AND a.startTime < :endTime)" +
            "GROUP BY a.eventType";

    private static final String FIND_BY_USER_ID = "SELECT " +
            "new net.therap.activitytracker.dto.SummaryDto(a.eventType, COUNT (a)" +
            ", AVG (FUNCTION('TIME_TO_SEC',FUNCTION ('TIMEDIFF',a.endTime , a.startTime))) / 3600)" +
            "FROM Activity a " +
            "WHERE (a.user.id = :userId)" +
            "GROUP BY a.eventType";

    public List<SummaryDto> findByUserId(long userId) {
        return em.createQuery(FIND_BY_USER_ID, SummaryDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    public List<SummaryDto> findByUserIdAndDuration(long userId, Date startTime, Date endTime) {
        return em.createQuery(FIND_BY_USER_AND_DURATION_QUERY, SummaryDto.class)
                .setParameter("userId", userId)
                .setParameter("startTime", startTime)
                .setParameter("endTime", endTime)
                .getResultList();
    }

    public List<ShortSummaryDto> findByDuration(Date startTime, Date endTime, int pageNo) {
        return em.createQuery(FIND_BY_DURATION_QUERY, ShortSummaryDto.class)
                .setParameter("startTime", startTime)
                .setParameter("endTime", endTime)
                .setFirstResult(PAGE_SIZE * pageNo)
                .setMaxResults(PAGE_SIZE)
                .getResultList();
    }

    public List<ShortSummaryDto> findAll(int pageNo) {
        return em.createQuery(FIND_ALL_QUERY, ShortSummaryDto.class)
                .setFirstResult(PAGE_SIZE * pageNo)
                .setMaxResults(PAGE_SIZE)
                .getResultList();
    }
}