package net.therap.activitytracker.service;

import net.therap.activitytracker.domain.Activity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

import static java.util.Objects.nonNull;
import static net.therap.activitytracker.constant.GlobalConstants.PAGE_SIZE;

/**
 * @author iftakhar.ahmed
 * @since 1/11/22
 */
@Service
public class ActivityService {

    @PersistenceContext
    private EntityManager em;

    public Activity find(long id) {
        return em.find(Activity.class, id);
    }

    @Transactional
    public Activity saveOrUpdate(Activity activity) {
        if (activity.isNew()) {
            em.persist(activity);
        } else {
            activity = em.merge(activity);
        }

        return activity;
    }

    @Transactional
    public void delete(Activity activity) {
        em.remove(em.getReference(Activity.class, activity.getId()));
    }

    public List<Activity> findAll(int pageNo) {
        return em.createNamedQuery("Activity.findAll", Activity.class)
                .setFirstResult(pageNo * PAGE_SIZE)
                .setMaxResults(PAGE_SIZE)
                .getResultList();
    }

    public List<Activity> findByEmail(String email, int pageNo) {
        return em.createNamedQuery("Activity.findByUserEmail", Activity.class)
                .setParameter("email", email)
                .setFirstResult(pageNo * PAGE_SIZE)
                .setMaxResults(PAGE_SIZE)
                .getResultList();
    }

    public boolean exists(long id) {
        return nonNull(find(id));
    }
}