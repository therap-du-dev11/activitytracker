package net.therap.activitytracker.service;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.util.HashGenerationUtil;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.List;

import static java.util.Objects.nonNull;
import static net.therap.activitytracker.constant.GlobalConstants.PAGE_SIZE;

/**
 * @author iftakhar.ahmed
 * @since 1/8/22
 */
@Service
public class UserService {

    @PersistenceContext
    private EntityManager em;

    public User find(long id) {
        return em.find(User.class, id);
    }

    @Transactional
    public User saveOrUpdate(User user) {
        if (user.isNew()) {
            user.setPassword(HashGenerationUtil.getMd5(user.getPassword()));
            em.persist(user);
        } else {
            user = em.merge(user);
        }

        return user;
    }

    @Transactional
    public User updatePassword(User user, String newPassword) {
        user.setPassword(HashGenerationUtil.getMd5(newPassword));
        user.setShouldChangePassword(false);

        return em.merge(user);
    }

    @Transactional
    public void delete(User user) {
        em.remove(em.getReference(User.class, user.getId()));
    }

    public List<User> findByName(String name, int pageNo) {
        return em.createNamedQuery("User.findByName", User.class)
                .setParameter("name", name)
                .setFirstResult(PAGE_SIZE * pageNo)
                .setMaxResults(PAGE_SIZE)
                .getResultList();
    }

    public User findByEmail(String email) {
        User user;

        try {
            user = em.createNamedQuery("User.findByEmail", User.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            user = null;
        }

        return user;
    }

    public boolean exists(long id) {
        return nonNull(find(id));
    }
}