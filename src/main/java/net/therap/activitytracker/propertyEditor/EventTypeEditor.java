package net.therap.activitytracker.propertyEditor;

import net.therap.activitytracker.domain.EventType;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

import static java.util.Objects.isNull;

/**
 * @author iftakhar.ahmed
 * @since 1/11/22
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EventTypeEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            EventType eventType = new EventType();
            eventType.setId(Long.parseLong(text));
            setValue(eventType);
        } catch (Exception e) {
            setValue(null);
        }
    }

    @Override
    public String getAsText() {
        EventType eventType = (EventType) getValue();
        return isNull(eventType) ? null : String.valueOf(eventType.getId());
    }
}