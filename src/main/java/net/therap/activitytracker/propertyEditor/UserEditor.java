package net.therap.activitytracker.propertyEditor;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

import static java.util.Objects.isNull;

/**
 * @author iftakhar.ahmed
 * @since 1/11/22
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserEditor extends PropertyEditorSupport {

    @Autowired
    private UserService userService;

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            User user = new User();
            user.setId(userService.findByEmail(text).getId());
            user.setEmail(text);
            setValue(user);
        } catch (Exception e) {
            setValue(null);
        }
    }

    @Override
    public String getAsText() {
        User user = (User) getValue();
        return isNull(user) ? null : user.getEmail();
    }
}