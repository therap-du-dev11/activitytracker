package net.therap.activitytracker.handler;

import net.therap.activitytracker.exception.AccessDeniedException;
import net.therap.activitytracker.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.OptimisticLockException;

import static net.therap.activitytracker.constant.GlobalConstants.ERROR_MESSAGE;
import static net.therap.activitytracker.constant.GlobalConstants.ERROR_PAGE;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private final Logger logger;

    @Autowired
    private MessageSourceAccessor msa;

    public GlobalExceptionHandler() {
        this.logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ModelAndView handleAccessDenied() {
        ModelAndView modelAndView = new ModelAndView(ERROR_PAGE);
        modelAndView.addObject(ERROR_MESSAGE, msa.getMessage("msg.err.access"));

        return modelAndView;
    }

    @ExceptionHandler(OptimisticLockException.class)
    public ModelAndView handleOptimisticLockException(OptimisticLockException e) {
        ModelAndView modelAndView = new ModelAndView(ERROR_PAGE);
        modelAndView.addObject(ERROR_MESSAGE, msa.getMessage("msg.err.optimisticLock"));

        logger.error("Parallel form update error:", e);

        return modelAndView;
    }

    @ExceptionHandler(HttpSessionRequiredException.class)
    public ModelAndView handleHttpSessionRequiredException(HttpSessionRequiredException e) {
        ModelAndView modelAndView = new ModelAndView(ERROR_PAGE);
        modelAndView.addObject(ERROR_MESSAGE, msa.getMessage("msg.err.setComplete"));

        logger.error("Parallel form update error:", e);

        return modelAndView;
    }

    @ExceptionHandler(CustomException.class)
    public ModelAndView handleCustomException(Exception e) {
        ModelAndView modelAndView = new ModelAndView(ERROR_PAGE);
        modelAndView.addObject(ERROR_MESSAGE, e.getMessage());

        logger.error("CUSTOM EXCEPTION", e);

        return modelAndView;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handle(Exception e) {
        ModelAndView modelAndView = new ModelAndView(ERROR_PAGE);
        modelAndView.addObject(ERROR_MESSAGE, msa.getMessage("err.whiteLevel"));

        logger.error("GLOBAL EXCEPTION: ", e);

        return modelAndView;
    }
}