package net.therap.activitytracker.exception;

/**
 * @author iftakhar.ahmed
 * @since 1/23/22
 */
public class CustomException extends RuntimeException{

    public CustomException(String message) {
        super(message);
    }
}