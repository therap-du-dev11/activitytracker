package net.therap.activitytracker.exception;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
public class InvalidStateException extends CustomException{

    public InvalidStateException(String message) {
        super(message);
    }
}