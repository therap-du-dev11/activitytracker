package net.therap.activitytracker.exception;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
public class AccessDeniedException extends CustomException {

    public AccessDeniedException(String message) {
        super(message);
    }
}