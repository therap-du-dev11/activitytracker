package net.therap.activitytracker.exception;

/**
 * @author iftakhar.ahmed
 * @since 1/20/22
 */
public class ResourceNotFoundException extends CustomException{

    public ResourceNotFoundException (String message) {
        super(message);
    }
}