package net.therap.activitytracker.config;

import com.opensymphony.sitemesh.webapp.SiteMeshFilter;
import net.therap.activitytracker.filter.AuthFilter;
import net.therap.activitytracker.filter.DuplicateLoginFilter;
import net.therap.activitytracker.filter.LoggingFilter;
import net.therap.activitytracker.filter.NewUserFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author iftakhar.ahmed
 * @since 1/10/22
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean<AuthFilter> authFilter() {
        FilterRegistrationBean<AuthFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new AuthFilter());
        registrationBean.addUrlPatterns("*");
        registrationBean.setOrder(2);

        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<DuplicateLoginFilter> duplicateLoginFilter() {
        FilterRegistrationBean<DuplicateLoginFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new DuplicateLoginFilter());
        registrationBean.addUrlPatterns("/auth/login");
        registrationBean.setOrder(4);

        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<NewUserFilter> newUserFilter() {
        FilterRegistrationBean<NewUserFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new NewUserFilter());
        registrationBean.addUrlPatterns("*");
        registrationBean.setOrder(3);

        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<LoggingFilter> loggingFilter() {
        FilterRegistrationBean<LoggingFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new LoggingFilter());
        registrationBean.addUrlPatterns("*");
        registrationBean.setOrder(1);

        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<SiteMeshFilter> siteMeshFilter() {
        FilterRegistrationBean<SiteMeshFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new SiteMeshFilter());
        registrationBean.addUrlPatterns("*");
        registrationBean.setOrder(5);

        return registrationBean;
    }
}