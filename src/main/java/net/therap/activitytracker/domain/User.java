package net.therap.activitytracker.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * @author iftakhar.ahmed
 * @since 1/4/22
 */
@NamedQueries({
        @NamedQuery(
                name = "User.findByEmail",
                query = "FROM User u WHERE u.email = :email"
        ),
        @NamedQuery(
                name = "User.findById",
                query = "SELECT u FROM User u LEFT JOIN FETCH u.roles WHERE u.id = :id"
        ),
        @NamedQuery(
                name = "User.findByName",
                query = "FROM User u WHERE u.name LIKE '%'||:name||'%' ORDER BY u.name"
        )
})
@Setter
@Getter
@Entity
@Table(name = "user")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User extends Persistent {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 3, max = 255)
    private String name;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Size(min = 3, max = 255)
    private String password;

    @Size(max = 255)
    private String phone;

    private boolean shouldChangePassword;

    @Size(min = 1)
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Activity> activities;

    public User() {
        this.shouldChangePassword = true;
        this.roles = new HashSet<>();
        this.activities = new HashSet<>();
    }
}