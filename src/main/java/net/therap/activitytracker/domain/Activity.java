package net.therap.activitytracker.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author iftakhar.ahmed
 * @since 1/4/22
 */
@NamedQueries({
        @NamedQuery(
                name = "Activity.findAll",
                query = "FROM Activity a ORDER BY a.user.name"
        ),
        @NamedQuery(
                name = "Activity.findByUserEmail",
                query = "FROM Activity a WHERE a.user.email = :email"
        )
})
@Setter
@Getter
@Entity
@Table(name = "activity")
public class Activity extends Persistent {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @Lob
    private String note;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "event_type_id")
    private EventType eventType;

    @ManyToMany
    @JoinTable(
            name = "related_person",
            joinColumns = @JoinColumn(name = "activity_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> relatedPersons;

    public Activity() {
        this.relatedPersons = new HashSet<>();
    }

    public Activity(User user) {
        this();
        this.user = user;
    }
}