package net.therap.activitytracker.domain;

/**
 * @author iftakhar.ahmed
 * @since 1/4/22
 */
public enum Role {

    ADMIN("Admin"),
    USER("User");

    private String displayName;

    private Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}