package net.therap.activitytracker.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * @author iftakhar.ahmed
 * @since 1/4/22
 */
@NamedQueries({
        @NamedQuery(
                name = "EventType.findAll",
                query = "FROM EventType et ORDER BY et.name"
        ),
        @NamedQuery(
                name = "EventType.findByName",
                query = "FROM EventType et WHERE et.name = :name"
        )
})
@Setter
@Getter
@Entity
@Table(name = "event_type")
public class EventType extends Persistent {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 3, max = 255)
    private String name;

    @Size(max = 2000)
    @Lob
    private String description;

    @OneToMany(mappedBy = "eventType", cascade = CascadeType.ALL)
    private Set<Activity> activities;

    public EventType() {
        this.activities = new HashSet<>();
    }
}