package net.therap.activitytracker.constant;

/**
 * @author iftakhar.ahmed
 * @since 1/8/22
 */
public class GlobalConstants {

    public static final String SUCCESS_REDIRECT_MESSAGE = "successRMsg";
    public static final String CURRENT_USER = "USER";
    public static final String IS_ADMIN = "isAdmin";
    public static final String DELETE = "DELETE";
    public static final String SAVE = "SAVE";
    public static final String UPDATE = "UPDATE";
    public static final String LOGIN = "LOGIN";
    public static final String LOGOUT = "LOGOUT";
    public static final String CRUD_LOG_FORMAT = "[Type= CRUD, operation = {} , on entity = {}, with id = {}]";
    public static final String IS_USER = "isUser";
    public static final String ERROR_MESSAGE = "errorMsg";
    public static final String ERROR_PAGE = "error/errorPage";
    public static final int PAGE_SIZE = 3;
}