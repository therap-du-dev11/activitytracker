package net.therap.activitytracker.validator;

import net.therap.activitytracker.command.PasswordCommand;
import net.therap.activitytracker.util.HashGenerationUtil;
import net.therap.activitytracker.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@Component
public class PasswordValidator implements Validator {

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return PasswordCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (errors.hasErrors()) {
            return;
        }

        PasswordCommand passwordCommand = (PasswordCommand) target;
        String hashedOldPassword = HashGenerationUtil.getMd5(passwordCommand.getOldPassword());
        if (!hashedOldPassword.equals(SessionUtil.getUser().getPassword())) {
            errors.rejectValue("oldPassword"
                    , "NOT_MATCHED", msa.getMessage("msg.err.password.auth"));
        }
        if (!passwordCommand.getNewPassword().equals(passwordCommand.getConfirmPassword())) {
            errors.rejectValue("confirmPassword"
                    , "NOT_MATCHED", msa.getMessage("msg.err.password.confirmation"));
        }
    }
}