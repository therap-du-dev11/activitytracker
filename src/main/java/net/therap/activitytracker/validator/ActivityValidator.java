package net.therap.activitytracker.validator;

import net.therap.activitytracker.domain.Activity;
import net.therap.activitytracker.command.ActivityCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static java.util.Objects.nonNull;

/**
 * @author iftakhar.ahmed
 * @since 1/16/22
 */
@Component
public class ActivityValidator implements Validator {

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return ActivityCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Activity activity = ((ActivityCommand) target).getActivity();

        if (nonNull(activity.getStartTime())
                && nonNull(activity.getEndTime())
                && activity.getEndTime().before(activity.getStartTime())) {
            errors.rejectValue("activity.endTime"
                    , "INCONSISTENCY", msa.getMessage("msg.warning.inconsistentTime"));
        }
    }
}