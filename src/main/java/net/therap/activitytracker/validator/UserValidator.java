package net.therap.activitytracker.validator;

import net.therap.activitytracker.domain.User;
import net.therap.activitytracker.service.UserService;
import net.therap.activitytracker.command.UserCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static java.util.Objects.nonNull;

/**
 * @author iftakhar.ahmed
 * @since 1/9/22
 */
@Component
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (errors.hasErrors()) {
            return;
        }

        User user = ((UserCommand) target).getUser();
        User oldUser = userService.findByEmail(user.getEmail());
        if (nonNull(oldUser) && !user.equals(oldUser)) {
            errors.rejectValue("user.email", "DUPLICATE", msa.getMessage("email.err.duplicate"));
        }
    }
}