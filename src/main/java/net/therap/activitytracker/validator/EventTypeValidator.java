package net.therap.activitytracker.validator;

import net.therap.activitytracker.domain.EventType;
import net.therap.activitytracker.service.EventTypeService;
import net.therap.activitytracker.command.EventTypeCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static java.util.Objects.nonNull;

/**
 * @author iftakhar.ahmed
 * @since 1/10/22
 */
@Component
public class EventTypeValidator implements Validator {

    @Autowired
    private EventTypeService eventTypeService;

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return EventTypeCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (errors.hasErrors()) {
            return;
        }

        EventType eventType = ((EventTypeCommand) target).getEventType();
        EventType oldEventType = eventTypeService.findByName(eventType.getName());
        if (nonNull(oldEventType) && !eventType.equals(oldEventType)) {
            errors.rejectValue("eventType.name"
                    , "DUPLICATE", msa.getMessage("name.err.duplicate"));
        }
    }
}